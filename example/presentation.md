[comment]: # (This presentation was made with markdown-slides)
[comment]: # (This is a CommonMark compliant comment. It will not be included in the presentation.)
[comment]: # (Compile this presentation with --include media)
[comment]: # (THEME = sky)
[comment]: # (CODE_THEME = zenburn)
[comment]: # (Pass optional settings to reveal.js:)
[comment]: # (keyboard: true)
[comment]: # (markdown: { smartypants: true })
[comment]: # (hash: false)
[comment]: # (respondToHashChanges: false)
[comment]: # (autoAnimate: true)
## Algoritmo Simplex y Método Gráfico
[comment]: # (!!! data-auto-animate)
## Algoritmo Simplex y Método Gráfico
Adrián Gutiérrez Gómez | UNAM | Enero 6, 2021
[comment]: # (!!! data-auto-animate)
### Método Gráfico

[comment]: # (!!! data-auto-animate)
### Método Gráfico

Incluye dos pasos:
[comment]: # (!!! data-auto-animate)
### Método Gráfico

Incluye dos pasos:

1. Determinar el espacio de soluciones factibles
[comment]: # (!!! data-auto-animate)
### Método Gráfico

Incluye dos pasos:

1. Determinar el espacio de soluciones factibles
2. Determinar la solución óptima de entre todos los puntos localizados en el espacio de soluciones
[comment]: # (!!! data-auto-animate)
Ejemplos:
[comment]: # (!!! data-auto-animate)
### Algoritmo Simplex

[comment]: # (!!! data-auto-animate)
### Algoritmo Simplex

Se imponen dos requerimientos a las restricciones de programación lineal
[comment]: # (!!! data-auto-animate)
### Algoritmo Simplex

Se imponen dos requerimientos a las restricciones de programación lineal

1. Todas las restricciones son ecuaciones con lado derecho no negativo
[comment]: # (!!! data-auto-animate)
### Algoritmo Simplex

Se imponen dos requerimientos a las restricciones de programación lineal

1. Todas las restricciones son ecuaciones con lado derecho no negativo
2.Todas las variables son no negativas
[comment]: # (!!! data-auto-animate)
### Conversión de las desigualdades en ecuaciones con lado derecho no negativo
[comment]: # (!!! data-auto-animate)
#### Conversión de las desigualdades en ecuaciones con lado derecho no negativo
- Para convertir una desigualdad (≤) en ecuación se agrega una variable de holgura al lado izquierdo de la restricción.
[comment]: # (!!! data-auto-animate)
#### Conversión de las desigualdades en ecuaciones con lado derecho no negativo
- Para convertir una desigualdad (≤) en ecuación se agrega una variable de holgura al lado izquierdo de la restricción.
`$$ 6 x_{1}+4x_{2}\leq24 $$`
[comment]: # (!!! data-auto-animate)
#### Conversión de las desigualdades en ecuaciones con lado derecho no negativo
- Para convertir una desigualdad (≤) en ecuación se agrega una variable de holgura al lado izquierdo de la restricción.
`$$ 6 x_{1}+4x_{2}+s_{1}=24 $$`
[comment]: # (!!! data-auto-animate)
#### Conversión de las desigualdades en ecuaciones con lado derecho no negativo
- Para convertir una desigualdad (≤) en ecuación se agrega una variable de holgura al lado izquierdo de la restricción.
`$$ 6 x_{1}+4x_{2}+s_{1}=24,s_{1}  \geq 0 $$`
[comment]: # (!!! data-auto-animate)
#### Conversión de las desigualdades en ecuaciones con lado derecho no negativo
- Para convertir una desigualdad (≤) en ecuación se agrega una variable de holgura al lado izquierdo de la restricción.
`$$ 6 x_{1}+4x_{2}+s_{1}=24,s_{1}  \geq 0 $$`
-Para convertir una restricción (≥) en ecuación se logra una variable de superávit no negativa del lado izquierdo de la desigualdad.
[comment]: # (!!! data-auto-animate)
#### Conversión de las desigualdades en ecuaciones con lado derecho no negativo
- Para convertir una desigualdad (≤) en ecuación se agrega una variable de holgura al lado izquierdo de la restricción.
`$$ 6 x_{1}+4x_{2}+s_{1}=24,s_{1}  \geq 0 $$`
-Para convertir una restricción (≥) en ecuación se logra una variable de superávit no negativa del lado izquierdo de la desigualdad.
-El lado derecho de la ecuación resultante debe ser no negativo.
[comment]: # (!!! data-auto-animate)
### Comparación entre ambos
![figura de comparación](media/ComparacionAmbos.png) <!-- .element: style="height:50vh; max-width:80vw;" -->

[comment]: # (!!! data-auto-animate)

